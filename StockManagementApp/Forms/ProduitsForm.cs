﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockManagementApp.Forms
{
    public partial class ProduitsForm : MetroFramework.Forms.MetroForm
    {
        private StockEntities DB;

        public ProduitsForm()
        {
            InitializeComponent();
            DB = new StockEntities();
        }

        public void AfficherListeProduits()
        {
            Categorie catg = new Categorie();
            foreach (var prod in DB.Produit)
            {
                catg = DB.Categorie.SingleOrDefault(c => c.Id == prod.Categorie_Id);
                if (catg != null)
                {
                    gridViewProduits.Rows.Add(prod.Id, prod.Nom, prod.Prix_Unitaire, prod.Nombre, prod.Date_Expiration, catg.Nom);
                }
            }
        }

        private void btAjouterProduit_Click(object sender, EventArgs e)
        {
            AjouterProduitForm form = new AjouterProduitForm();
            form.ShowDialog();
        }

        private void ProduitsForm_Load(object sender, EventArgs e)
        {
            AfficherListeProduits();
        }
    }
}
