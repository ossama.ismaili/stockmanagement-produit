﻿
namespace StockManagementApp.Forms
{
    partial class AjouterProduitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.cbProduitCategorie = new System.Windows.Forms.ComboBox();
            this.dateExpitation = new MetroFramework.Controls.MetroDateTime();
            this.tbProduitNombre = new MetroFramework.Controls.MetroTextBox();
            this.tbProduitPU = new MetroFramework.Controls.MetroTextBox();
            this.tbProduitNom = new MetroFramework.Controls.MetroTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btParcourir = new MetroFramework.Controls.MetroButton();
            this.imageProduit = new System.Windows.Forms.PictureBox();
            this.btAnnuler = new MetroFramework.Controls.MetroButton();
            this.btEnregistrer = new MetroFramework.Controls.MetroButton();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageProduit)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.cbProduitCategorie);
            this.metroPanel1.Controls.Add(this.dateExpitation);
            this.metroPanel1.Controls.Add(this.tbProduitNombre);
            this.metroPanel1.Controls.Add(this.tbProduitPU);
            this.metroPanel1.Controls.Add(this.tbProduitNom);
            this.metroPanel1.Controls.Add(this.label6);
            this.metroPanel1.Controls.Add(this.label5);
            this.metroPanel1.Controls.Add(this.label4);
            this.metroPanel1.Controls.Add(this.label3);
            this.metroPanel1.Controls.Add(this.label2);
            this.metroPanel1.Controls.Add(this.btParcourir);
            this.metroPanel1.Controls.Add(this.imageProduit);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(35, 63);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(538, 252);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // cbProduitCategorie
            // 
            this.cbProduitCategorie.FormattingEnabled = true;
            this.cbProduitCategorie.Location = new System.Drawing.Point(269, 46);
            this.cbProduitCategorie.Name = "cbProduitCategorie";
            this.cbProduitCategorie.Size = new System.Drawing.Size(248, 21);
            this.cbProduitCategorie.TabIndex = 15;
            // 
            // dateExpitation
            // 
            this.dateExpitation.Location = new System.Drawing.Point(269, 136);
            this.dateExpitation.MinimumSize = new System.Drawing.Size(0, 29);
            this.dateExpitation.Name = "dateExpitation";
            this.dateExpitation.Size = new System.Drawing.Size(200, 29);
            this.dateExpitation.TabIndex = 14;
            // 
            // tbProduitNombre
            // 
            // 
            // 
            // 
            this.tbProduitNombre.CustomButton.Image = null;
            this.tbProduitNombre.CustomButton.Location = new System.Drawing.Point(226, 1);
            this.tbProduitNombre.CustomButton.Name = "";
            this.tbProduitNombre.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbProduitNombre.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbProduitNombre.CustomButton.TabIndex = 1;
            this.tbProduitNombre.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbProduitNombre.CustomButton.UseSelectable = true;
            this.tbProduitNombre.CustomButton.Visible = false;
            this.tbProduitNombre.Lines = new string[0];
            this.tbProduitNombre.Location = new System.Drawing.Point(269, 106);
            this.tbProduitNombre.MaxLength = 32767;
            this.tbProduitNombre.Name = "tbProduitNombre";
            this.tbProduitNombre.PasswordChar = '\0';
            this.tbProduitNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbProduitNombre.SelectedText = "";
            this.tbProduitNombre.SelectionLength = 0;
            this.tbProduitNombre.SelectionStart = 0;
            this.tbProduitNombre.ShortcutsEnabled = true;
            this.tbProduitNombre.Size = new System.Drawing.Size(248, 23);
            this.tbProduitNombre.TabIndex = 13;
            this.tbProduitNombre.UseSelectable = true;
            this.tbProduitNombre.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbProduitNombre.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbProduitPU
            // 
            // 
            // 
            // 
            this.tbProduitPU.CustomButton.Image = null;
            this.tbProduitPU.CustomButton.Location = new System.Drawing.Point(226, 1);
            this.tbProduitPU.CustomButton.Name = "";
            this.tbProduitPU.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbProduitPU.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbProduitPU.CustomButton.TabIndex = 1;
            this.tbProduitPU.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbProduitPU.CustomButton.UseSelectable = true;
            this.tbProduitPU.CustomButton.Visible = false;
            this.tbProduitPU.Lines = new string[0];
            this.tbProduitPU.Location = new System.Drawing.Point(269, 76);
            this.tbProduitPU.MaxLength = 32767;
            this.tbProduitPU.Name = "tbProduitPU";
            this.tbProduitPU.PasswordChar = '\0';
            this.tbProduitPU.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbProduitPU.SelectedText = "";
            this.tbProduitPU.SelectionLength = 0;
            this.tbProduitPU.SelectionStart = 0;
            this.tbProduitPU.ShortcutsEnabled = true;
            this.tbProduitPU.Size = new System.Drawing.Size(248, 23);
            this.tbProduitPU.TabIndex = 12;
            this.tbProduitPU.UseSelectable = true;
            this.tbProduitPU.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbProduitPU.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbProduitNom
            // 
            // 
            // 
            // 
            this.tbProduitNom.CustomButton.Image = null;
            this.tbProduitNom.CustomButton.Location = new System.Drawing.Point(226, 1);
            this.tbProduitNom.CustomButton.Name = "";
            this.tbProduitNom.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbProduitNom.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbProduitNom.CustomButton.TabIndex = 1;
            this.tbProduitNom.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbProduitNom.CustomButton.UseSelectable = true;
            this.tbProduitNom.CustomButton.Visible = false;
            this.tbProduitNom.Lines = new string[0];
            this.tbProduitNom.Location = new System.Drawing.Point(269, 16);
            this.tbProduitNom.MaxLength = 32767;
            this.tbProduitNom.Name = "tbProduitNom";
            this.tbProduitNom.PasswordChar = '\0';
            this.tbProduitNom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbProduitNom.SelectedText = "";
            this.tbProduitNom.SelectionLength = 0;
            this.tbProduitNom.SelectionStart = 0;
            this.tbProduitNom.ShortcutsEnabled = true;
            this.tbProduitNom.Size = new System.Drawing.Size(248, 23);
            this.tbProduitNom.TabIndex = 11;
            this.tbProduitNom.UseSelectable = true;
            this.tbProduitNom.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbProduitNom.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(174, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Date d\'Expiration";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(174, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Nombre";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(174, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Prix Unitaire";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(174, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Categorie";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(174, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nom";
            // 
            // btParcourir
            // 
            this.btParcourir.BackColor = System.Drawing.Color.White;
            this.btParcourir.Location = new System.Drawing.Point(45, 168);
            this.btParcourir.Name = "btParcourir";
            this.btParcourir.Size = new System.Drawing.Size(75, 23);
            this.btParcourir.TabIndex = 3;
            this.btParcourir.Text = "Parcourir";
            this.btParcourir.UseSelectable = true;
            this.btParcourir.Click += new System.EventHandler(this.btParcourir_Click);
            // 
            // imageProduit
            // 
            this.imageProduit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageProduit.Location = new System.Drawing.Point(17, 18);
            this.imageProduit.Name = "imageProduit";
            this.imageProduit.Size = new System.Drawing.Size(138, 144);
            this.imageProduit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageProduit.TabIndex = 2;
            this.imageProduit.TabStop = false;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(316, 330);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(115, 35);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseSelectable = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.BackColor = System.Drawing.Color.Transparent;
            this.btEnregistrer.Location = new System.Drawing.Point(458, 330);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(115, 35);
            this.btEnregistrer.TabIndex = 5;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseSelectable = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // AjouterProduitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 376);
            this.Controls.Add(this.btEnregistrer);
            this.Controls.Add(this.btAnnuler);
            this.Controls.Add(this.metroPanel1);
            this.Name = "AjouterProduitForm";
            this.Text = "Ajouter Produit";
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageProduit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroButton btParcourir;
        private System.Windows.Forms.PictureBox imageProduit;
        private System.Windows.Forms.ComboBox cbProduitCategorie;
        private MetroFramework.Controls.MetroDateTime dateExpitation;
        private MetroFramework.Controls.MetroTextBox tbProduitNombre;
        private MetroFramework.Controls.MetroTextBox tbProduitPU;
        private MetroFramework.Controls.MetroTextBox tbProduitNom;
        private MetroFramework.Controls.MetroButton btAnnuler;
        private MetroFramework.Controls.MetroButton btEnregistrer;
    }
}