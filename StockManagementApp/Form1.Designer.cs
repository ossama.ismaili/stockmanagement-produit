﻿
namespace StockManagementApp
{
    partial class StockManagenemtApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.sidePanel = new System.Windows.Forms.Panel();
            this.aProposBtn = new FontAwesome.Sharp.IconButton();
            this.usersBtn = new FontAwesome.Sharp.IconButton();
            this.fournisseursBtn = new FontAwesome.Sharp.IconButton();
            this.produitsBtn = new FontAwesome.Sharp.IconButton();
            this.dashboardBtn = new FontAwesome.Sharp.IconButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.iconButton5 = new FontAwesome.Sharp.IconButton();
            this.iconButton6 = new FontAwesome.Sharp.IconButton();
            this.iconButton7 = new FontAwesome.Sharp.IconButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.btFormProduit = new MetroFramework.Controls.MetroButton();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.sidePanel);
            this.panel1.Controls.Add(this.aProposBtn);
            this.panel1.Controls.Add(this.usersBtn);
            this.panel1.Controls.Add(this.fournisseursBtn);
            this.panel1.Controls.Add(this.produitsBtn);
            this.panel1.Controls.Add(this.dashboardBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 620);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(21, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nom utilisateur/Fonction";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Icon personne";
            // 
            // sidePanel
            // 
            this.sidePanel.BackColor = System.Drawing.Color.Maroon;
            this.sidePanel.Location = new System.Drawing.Point(0, 152);
            this.sidePanel.Name = "sidePanel";
            this.sidePanel.Size = new System.Drawing.Size(10, 54);
            this.sidePanel.TabIndex = 2;
            // 
            // aProposBtn
            // 
            this.aProposBtn.FlatAppearance.BorderSize = 0;
            this.aProposBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aProposBtn.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aProposBtn.ForeColor = System.Drawing.Color.White;
            this.aProposBtn.IconChar = FontAwesome.Sharp.IconChar.Question;
            this.aProposBtn.IconColor = System.Drawing.Color.Black;
            this.aProposBtn.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.aProposBtn.IconSize = 30;
            this.aProposBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.aProposBtn.Location = new System.Drawing.Point(8, 392);
            this.aProposBtn.Name = "aProposBtn";
            this.aProposBtn.Size = new System.Drawing.Size(192, 54);
            this.aProposBtn.TabIndex = 3;
            this.aProposBtn.Text = "A propos";
            this.aProposBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.aProposBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.aProposBtn.UseVisualStyleBackColor = true;
            this.aProposBtn.Click += new System.EventHandler(this.aProposBtn_Click);
            // 
            // usersBtn
            // 
            this.usersBtn.FlatAppearance.BorderSize = 0;
            this.usersBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.usersBtn.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usersBtn.ForeColor = System.Drawing.Color.White;
            this.usersBtn.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.usersBtn.IconColor = System.Drawing.Color.Black;
            this.usersBtn.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.usersBtn.IconSize = 30;
            this.usersBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.usersBtn.Location = new System.Drawing.Point(8, 332);
            this.usersBtn.Name = "usersBtn";
            this.usersBtn.Size = new System.Drawing.Size(192, 54);
            this.usersBtn.TabIndex = 3;
            this.usersBtn.Text = "Utilisateurs??";
            this.usersBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.usersBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.usersBtn.UseVisualStyleBackColor = true;
            this.usersBtn.Click += new System.EventHandler(this.usersBtn_Click);
            // 
            // fournisseursBtn
            // 
            this.fournisseursBtn.FlatAppearance.BorderSize = 0;
            this.fournisseursBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fournisseursBtn.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fournisseursBtn.ForeColor = System.Drawing.Color.White;
            this.fournisseursBtn.IconChar = FontAwesome.Sharp.IconChar.Users;
            this.fournisseursBtn.IconColor = System.Drawing.Color.Black;
            this.fournisseursBtn.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.fournisseursBtn.IconSize = 30;
            this.fournisseursBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fournisseursBtn.Location = new System.Drawing.Point(8, 272);
            this.fournisseursBtn.Name = "fournisseursBtn";
            this.fournisseursBtn.Size = new System.Drawing.Size(192, 54);
            this.fournisseursBtn.TabIndex = 3;
            this.fournisseursBtn.Text = "Fournisseurs";
            this.fournisseursBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fournisseursBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.fournisseursBtn.UseVisualStyleBackColor = true;
            this.fournisseursBtn.Click += new System.EventHandler(this.fournisseursBtn_Click);
            // 
            // produitsBtn
            // 
            this.produitsBtn.FlatAppearance.BorderSize = 0;
            this.produitsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.produitsBtn.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.produitsBtn.ForeColor = System.Drawing.Color.White;
            this.produitsBtn.IconChar = FontAwesome.Sharp.IconChar.List;
            this.produitsBtn.IconColor = System.Drawing.Color.Black;
            this.produitsBtn.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.produitsBtn.IconSize = 30;
            this.produitsBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.produitsBtn.Location = new System.Drawing.Point(8, 212);
            this.produitsBtn.Name = "produitsBtn";
            this.produitsBtn.Size = new System.Drawing.Size(192, 54);
            this.produitsBtn.TabIndex = 3;
            this.produitsBtn.Text = "Produits";
            this.produitsBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.produitsBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.produitsBtn.UseVisualStyleBackColor = true;
            this.produitsBtn.Click += new System.EventHandler(this.produitsBtn_Click);
            // 
            // dashboardBtn
            // 
            this.dashboardBtn.FlatAppearance.BorderSize = 0;
            this.dashboardBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dashboardBtn.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dashboardBtn.ForeColor = System.Drawing.Color.White;
            this.dashboardBtn.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.dashboardBtn.IconColor = System.Drawing.Color.Black;
            this.dashboardBtn.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.dashboardBtn.IconSize = 30;
            this.dashboardBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.dashboardBtn.Location = new System.Drawing.Point(8, 152);
            this.dashboardBtn.Name = "dashboardBtn";
            this.dashboardBtn.Size = new System.Drawing.Size(192, 54);
            this.dashboardBtn.TabIndex = 3;
            this.dashboardBtn.Text = "Dashboard";
            this.dashboardBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.dashboardBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dashboardBtn.UseVisualStyleBackColor = true;
            this.dashboardBtn.Click += new System.EventHandler(this.dashboardBtn_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkRed;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(200, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(923, 10);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(103, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Gestion Stock";
            // 
            // iconButton5
            // 
            this.iconButton5.BackColor = System.Drawing.Color.White;
            this.iconButton5.FlatAppearance.BorderSize = 0;
            this.iconButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton5.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconButton5.ForeColor = System.Drawing.Color.Black;
            this.iconButton5.IconChar = FontAwesome.Sharp.IconChar.PowerOff;
            this.iconButton5.IconColor = System.Drawing.Color.Silver;
            this.iconButton5.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButton5.IconSize = 32;
            this.iconButton5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.iconButton5.Location = new System.Drawing.Point(1071, 16);
            this.iconButton5.Name = "iconButton5";
            this.iconButton5.Size = new System.Drawing.Size(40, 35);
            this.iconButton5.TabIndex = 4;
            this.iconButton5.UseVisualStyleBackColor = false;
            this.iconButton5.Click += new System.EventHandler(this.iconButton5_Click);
            // 
            // iconButton6
            // 
            this.iconButton6.BackColor = System.Drawing.Color.White;
            this.iconButton6.FlatAppearance.BorderSize = 0;
            this.iconButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton6.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconButton6.ForeColor = System.Drawing.Color.White;
            this.iconButton6.IconChar = FontAwesome.Sharp.IconChar.Bell;
            this.iconButton6.IconColor = System.Drawing.Color.Silver;
            this.iconButton6.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButton6.IconSize = 32;
            this.iconButton6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.iconButton6.Location = new System.Drawing.Point(1025, 16);
            this.iconButton6.Name = "iconButton6";
            this.iconButton6.Size = new System.Drawing.Size(40, 35);
            this.iconButton6.TabIndex = 4;
            this.iconButton6.UseVisualStyleBackColor = false;
            // 
            // iconButton7
            // 
            this.iconButton7.BackColor = System.Drawing.Color.White;
            this.iconButton7.FlatAppearance.BorderSize = 0;
            this.iconButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton7.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconButton7.ForeColor = System.Drawing.Color.White;
            this.iconButton7.IconChar = FontAwesome.Sharp.IconChar.Cog;
            this.iconButton7.IconColor = System.Drawing.Color.Silver;
            this.iconButton7.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButton7.IconSize = 32;
            this.iconButton7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.iconButton7.Location = new System.Drawing.Point(979, 16);
            this.iconButton7.Name = "iconButton7";
            this.iconButton7.Size = new System.Drawing.Size(40, 35);
            this.iconButton7.TabIndex = 4;
            this.iconButton7.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkRed;
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Location = new System.Drawing.Point(241, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(214, 51);
            this.panel4.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "LOGO";
            // 
            // btFormProduit
            // 
            this.btFormProduit.Location = new System.Drawing.Point(250, 100);
            this.btFormProduit.Name = "btFormProduit";
            this.btFormProduit.Size = new System.Drawing.Size(199, 47);
            this.btFormProduit.TabIndex = 6;
            this.btFormProduit.Text = "Produit";
            this.btFormProduit.UseSelectable = true;
            this.btFormProduit.Click += new System.EventHandler(this.btFormProduit_Click);
            // 
            // StockManagenemtApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1123, 620);
            this.Controls.Add(this.btFormProduit);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.iconButton7);
            this.Controls.Add(this.iconButton6);
            this.Controls.Add(this.iconButton5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "StockManagenemtApp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private FontAwesome.Sharp.IconButton dashboardBtn;
        private System.Windows.Forms.Panel sidePanel;
        private FontAwesome.Sharp.IconButton usersBtn;
        private FontAwesome.Sharp.IconButton fournisseursBtn;
        private FontAwesome.Sharp.IconButton produitsBtn;
        private FontAwesome.Sharp.IconButton iconButton5;
        private FontAwesome.Sharp.IconButton iconButton6;
        private FontAwesome.Sharp.IconButton iconButton7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private FontAwesome.Sharp.IconButton aProposBtn;
        private System.Windows.Forms.Label label4;
        private MetroFramework.Controls.MetroButton btFormProduit;
    }
}

